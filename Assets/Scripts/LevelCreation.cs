﻿using UnityEngine;
using System;
using System.Collections.Generic;


public partial class Level
{

    void CreateLevel()
    {
        for (int x = 0; x < 20; ++x)
        {
            AddEmptyColumn();
        }

        while (mines.Count < Columns)
        {
            AddColumn();
        }
    }

    void RemoveColumn()
    {
        Mine[] column = mines[0];
        for (int y = 0; y < Rows; ++y)
        {
            Destroy(column[y].gameObject);
        }

        mines.RemoveAt(0);
        deletedColumnOffset++;
    }

    public void AddColumn()
    {
        Mine[] genM2 = mines[mines.Count - 4];
        Mine[] genM1 = mines[mines.Count - 3];
        Mine[] gen0 = mines[mines.Count - 2];
        Mine[] genP1 = mines[mines.Count - 1];

        PlaceMines(genM1, gen0, genP1);
        ContinueOpenness(genM2, genM1);
        AddEmptyColumn();
    }

    Mine[] AddEmptyColumn()
    {
        int x = columnsCreated++;
        Mine[] column = new Mine[Rows];
        for (int y = 0; y < Rows; ++y)
        {
            MineData mineData = new MineData();
            mineData.Level = this;
            mineData.X = x;
            mineData.Y = y;
            mineData.CoverSprite = FieldClosed;
            mineData.MarkedSprite = FieldMarked;
            mineData.IsMarked = false;
            mineData.IsOpen = false;

            GameObject mineObject = Instantiate(FieldPrefab);
            column[y] = mineObject.GetComponent<Mine>();
            column[y].Initialize(mineData, LevelTranslator.transform, x * this.transform.localScale.x, y * this.transform.localScale.y);
        }
        mines.Add(column);
        return column;
    }

    void PlaceMines(Mine[] prev, Mine[] curr, Mine[] next)
    {
        for (int y = 0; y < curr.Length; ++y)
        {
            if (random.Next(0, 20) == 0)
            {
                curr[y].MineData.IsMine = true;
                if (y > 0)
                {
                    curr[y - 1].MineData.SurroundingMines += 1;
                    if (prev != null) prev[y - 1].MineData.SurroundingMines += 1;
                    if (next != null) next[y - 1].MineData.SurroundingMines += 1;
                }
                if (y + 1 < curr.Length)
                {
                    curr[y + 1].MineData.SurroundingMines += 1;
                    if (prev != null) prev[y + 1].MineData.SurroundingMines += 1;
                    if (next != null) next[y + 1].MineData.SurroundingMines += 1;
                }
                if (prev != null) prev[y].MineData.SurroundingMines += 1;
                if (next != null) next[y].MineData.SurroundingMines += 1;
            }
        }
    }

    void ContinueOpenness(Mine[] prev, Mine[] curr)
    {
        for (int y = 0; y < curr.Length; ++y)
        {
            if (prev[y].MineData.IsOpen && prev[y].MineData.SurroundingMines == 0)
            {
                if (y > 0)
                {
                    curr[y - 1].Open();
                }
                curr[y].Open();
                if (y + 1 < curr.Length)
                {
                    curr[y + 1].Open();
                }
            }
        }
    }

}

