﻿using UnityEngine;


public abstract class MouseEventReceiver : MonoBehaviour
{
    virtual public void OnMouseLeftClicked() { }
    virtual public void OnMouseRightClicked() { }
    virtual public void OnMouseMiddleClicked() { }

    private bool mouseDownLeft = false;
    private bool mouseDownRight = false;
    private bool mouseDownMiddle = false;

    private bool mouseBlockLeft = false;
    private bool mouseBlockRight = false;
    private bool mouseBlockMiddle = false;

    public void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            if (!mouseBlockLeft)
            {
                mouseDownLeft = true;
            }
        }
        else
        {
            if (mouseDownLeft)
            {
                OnMouseLeftClicked();
            }
            mouseBlockLeft = false;
            mouseDownLeft = false;
        }

        if (Input.GetMouseButton(1))
        {
            if (!mouseBlockRight)
            {
                mouseDownRight = true;
            }
        }
        else
        {
            if (mouseDownRight)
            {
                OnMouseRightClicked();
            }
            mouseBlockRight = false;
            mouseDownRight = false;
        }

        if (Input.GetMouseButton(2))
        {
            if (!mouseBlockMiddle)
            {
                mouseDownMiddle = true;
            }
        }
        else
        {
            if (mouseDownMiddle)
            {
                OnMouseMiddleClicked();
            }
            mouseBlockMiddle = false;
            mouseDownMiddle = false;
        }
    }

    public void OnMouseEnter()
    {
        mouseBlockLeft = Input.GetMouseButton(0);
        mouseBlockRight = Input.GetMouseButton(1);
        mouseBlockMiddle = Input.GetMouseButton(2);
    }

    public void OnMouseExit()
    {
        mouseDownLeft = false;
        mouseDownRight = false;
        mouseDownMiddle = false;
    }
}

