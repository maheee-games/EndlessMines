﻿using UnityEngine;
using System;
using System.Collections.Generic;


public partial class Level
{

    void CreateScanner()
    {
        scannerFields = new GameObject[Rows];
        for (int y = 0; y < Rows; ++y)
        {
            scannerFields[y] = Instantiate(DeadFieldPrefab);

            scannerFields[y].transform.SetParent(this.transform, false);
            scannerFields[y].transform.Translate(this.transform.localScale.x + ScannerOffset, y * this.transform.localScale.y, -0.5f);
        }
    }

    void ResetScanner()
    {
        for (int y = 0; y < Rows; ++y)
        {
            scannerFields[y].GetComponent<SpriteRenderer>().sprite = ScannerEmpty;
        }
    }

    void ScanFirstColumn()
    {
        int cleared = 0;
        int missed = 0;
        int mistakenlyMarked = 0;

        Mine[] column = mines[0];

        for (int y = 0; y < Rows; ++y)
        {
            if (column[y].MineData.IsMarked)
            {
                if (column[y].MineData.IsMine)
                {
                    scannerFields[y].GetComponent<SpriteRenderer>().sprite = ScannerMarkOk;
                    ++cleared;
                }
                else
                {
                    scannerFields[y].GetComponent<SpriteRenderer>().sprite = ScannerMarkFailure;
                    ++mistakenlyMarked;
                }
            }
            else
            {
                if (column[y].MineData.IsMine)
                {
                    scannerFields[y].GetComponent<SpriteRenderer>().sprite = ScannerMissedMine;
                    if (!column[y].MineData.IsOpen)
                    {
                        ++missed;
                    }
                }
                else
                {
                    scannerFields[y].GetComponent<SpriteRenderer>().sprite = ScannerEmpty;
                }
            }
        }

        game.OnRowPassed(cleared, missed, mistakenlyMarked);
    }
}

