﻿using UnityEngine;
using UnityEngine.UI;

public class ScrollingText : MonoBehaviour {

    public int MaxTitleTextLength = 10;
    public string Text = "";
    public string StartText = "";

    private Text textComponent;
    private string currentText;

    void Start () {
        textComponent = GetComponent<Text>();
        InvokeRepeating("UpdateText", 0.0f, 0.5f);
        currentText = StartText;
    }
	
	void Update () {
	
	}

    void UpdateText()
    {
        if (currentText.Length > 0)
        {
            currentText = currentText.Substring(1);
        }
        while (currentText.Length < MaxTitleTextLength)
        {
            currentText += "  " + Text;
        }
        textComponent.text = currentText.Substring(0, MaxTitleTextLength);
    }

    public void SetText(string text, bool reset)
    {
        this.Text = text;
        if (reset)
        {
            currentText = "        ";
        }
    }
}
