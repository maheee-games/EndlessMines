﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Globalization;

public class Game : MonoBehaviour {

    public GameObject[] LivesObjects = new GameObject[5];
    public Sprite LifeOk;
    public Sprite LifeLost;

    private Level level;
    private ScrollingText titleText;
    private Text scoreText;

    private int lives = 5;
    private int score = 0;

    private const string TEXT_START = "Press Space to Start";
    private const string TEXT_GAME = "Don't Touch the Mines!";

    private NumberFormatInfo nfi;

    void OnValidate()
    {
        if (LivesObjects.Length != 5)
        {
            Debug.LogWarning("Don't change the field's array size!");
            Array.Resize(ref LivesObjects, 9);
        }
    }

    void Start () {
        nfi = new NumberFormatInfo();
        nfi.NumberDecimalDigits = 0;
        nfi.NumberDecimalSeparator = " ";
        nfi.NumberGroupSeparator = ".";

        level = GetComponentInChildren<Level>();
        titleText = GameObject.Find("Title Text").GetComponent<ScrollingText>();
        scoreText = GameObject.Find("Score Text").GetComponent<Text>();

        UpdateLifeDisplay();
        titleText.SetText(TEXT_START, true);

        level.Initialize();
    }
	
	void Update() {
        if (level.Running)
        {
            if (Input.GetKey("space"))
            {
                level.Turbo = true;
            }
            else
            {
                level.Turbo = false;
            }
        }
        else
        {
            if (Input.GetKeyUp("space"))
            {
                StartGame();
            }
        }
    }

    void StartGame()
    {
        lives = 5;
        score = 0;
        IncreaseScore(0);
        UpdateLifeDisplay();

        level.Reset();
        level.OpenFirstColumn();
        level.Clickable = true;
        level.Running = true;

        titleText.SetText(TEXT_GAME, true);
    }

    void StopGame()
    {
        level.Clickable = false;
        level.Running = false;

        titleText.SetText(TEXT_START, true);
    }

    void UpdateLifeDisplay()
    {
        int i = 0;
        for (; i < LivesObjects.Length && i < lives; ++i)
        {
            LivesObjects[i].GetComponent<SpriteRenderer>().sprite = LifeOk;
        }
        for (; i < LivesObjects.Length; ++i)
        {
            LivesObjects[i].GetComponent<SpriteRenderer>().sprite = LifeLost;
        }
    }

    void RemoveLife()
    {
        --lives;
        if (lives <= 0)
        {
            lives = 0;
            StopGame();
        }
        UpdateLifeDisplay();
    }

    void IncreaseScore(int amount)
    {
        score += amount;
        scoreText.text = score.ToString("N", nfi);
    }

    public void OnRowPassed(int amountCleared, int amountMissed, int amountMistakenlyMarked)
    {
        if (amountMissed > 0 || amountMistakenlyMarked > 0)
        {
            RemoveLife();
        }
        IncreaseScore(1);
    }

    public void OnMineExploded()
    {
        RemoveLife();
    }
}
