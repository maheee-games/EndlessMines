﻿using UnityEngine;


public class MineData
{
    public Level Level { get; set; }
    public int X { get; set; }
    public int Y { get; set; }

    public bool IsOpen { get; set; }
    public bool IsMarked { get; set; }

    public bool IsMine { get; set; }
    public int SurroundingMines { get; set; }

    public Sprite CoverSprite { get; set; }
    public Sprite MarkedSprite { get; set; }
    public Sprite OpenSprite
    {
        get
        {
            return Level.GetOpenSprite(this);
        }
        set { }
    }

    public int Value { get; set; }
}

