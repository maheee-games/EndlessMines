﻿using UnityEngine;
using System;
using System.Collections.Generic;


public partial class Level
{

    public Sprite GetOpenSprite(MineData mineData)
    {
        if (mineData.IsMine)
        {
            return FieldMine;
        }
        return Field[mineData.SurroundingMines];
    }

    Mine GetMine(int x, int y)
    {
        if (x < 0 || x >= mines.Count)
        {
            return null;
        }
        Mine[] column = mines[x];
        if (y < 0 || y >= column.Length)
        {
            return null;
        }
        return column[y];
    }

}

