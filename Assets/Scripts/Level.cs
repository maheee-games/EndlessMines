﻿using UnityEngine;
using System;
using System.Collections.Generic;


public partial class Level : MonoBehaviour
{

    public GameObject DeadFieldPrefab;
    public GameObject FieldPrefab;
    public GameObject LevelTranslator;

    public Sprite[] Field = new Sprite[9];
    public Sprite FieldClosed;
    public Sprite FieldMine;
    public Sprite FieldMarked;

    public int Rows = 5;
    public int Columns = 5;

    public Sprite ScannerEmpty;
    public Sprite ScannerMarkOk;
    public Sprite ScannerMarkFailure;
    public Sprite ScannerMissedMine;
    public float ScannerOffset = -3.0f;

    private List<Mine[]> mines = new List<Mine[]>();
    private GameObject[] scannerFields;

    private System.Random random = new System.Random();

    private int columnsCreated = 0;
    private int columnsGone = 0;
    private int deletedColumnOffset = 0;
    private float speed = 0.5f;
    private float turboSpeed = 2.0f;

    private Game game;

    public bool Running;
    public bool Turbo;
    public bool Clickable;

    void OnValidate()
    {
        if (Field.Length != 9)
        {
            Debug.LogWarning("Don't change the field's array size!");
            Array.Resize(ref Field, 9);
        }
    }

    public void Initialize()
    {
        game = GetComponentInParent<Game>();
        CreateScanner();
        Reset();
    }

    public void Reset()
    {
        while (mines.Count > 0)
        {
            RemoveColumn();
        }

        columnsCreated = 0;
        columnsGone = 0;
        deletedColumnOffset = 0;

        LevelTranslator.transform.localPosition = new Vector3(0, 0, 0);

        CreateLevel();
        ResetScanner();
    }

    public void OpenFirstColumn()
    {
        Mine[] column = mines[0];
        for (int y = 0; y < Rows; ++y)
        {
            column[y].Open();
        }
    }

    void Update()
    {
        if (!Running)
        {
            return;
        }
        float s = (Turbo ? turboSpeed : speed);

        float t = -Time.deltaTime * s * this.transform.localScale.x;
        LevelTranslator.transform.Translate(t, 0, 0);

        int nowGone = (int) -LevelTranslator.transform.localPosition.x;
        if (nowGone > columnsGone)
        {
            ScanFirstColumn();
            RemoveColumn();
            AddColumn();
            columnsGone = nowGone;
        }
    }

    public void OnMineLeftClicked(Mine mine)
    {
        if (Clickable && !mine.MineData.IsOpen && !mine.MineData.IsMarked)
        {
            mine.Open();
        }
    }

    public void OnMineRightClicked(Mine mine)
    {
        if (Clickable && !mine.MineData.IsOpen)
        {
            mine.ToggleMark();
        }
    }

    public void OnMineMiddleClicked(Mine mine)
    {
        if (Clickable && mine.MineData.IsOpen)
        {
            OpenSurrounding(mine.MineData.X - deletedColumnOffset, mine.MineData.Y);
        }
    }

    public void OnMineOpening(Mine mine)
    {
        if (mine != null && !mine.MineData.IsMine && mine.MineData.SurroundingMines == 0)
        {
            OpenSurrounding(mine.MineData.X - deletedColumnOffset, mine.MineData.Y);
        }
        if (mine != null && mine.MineData.IsMine)
        {
            game.OnMineExploded();
        }
    }

    void Open(int x, int y)
    {
        Mine mine = GetMine(x, y);
        Open(mine);
    }

    void Open(Mine mine)
    {
        if (mine == null || mine.MineData.IsOpen)
        {
            return;
        }
        mine.Open();
    }

    void OpenSurrounding(int x, int y)
    {
        for (int ix = x - 1; ix <= x + 1; ++ix)
        {
            for (int iy = y - 1; iy <= y + 1; ++iy)
            {
                // don't open center
                if (ix == x && iy == y)
                {
                    continue;
                }
                // don't open last row
                if (ix >= mines.Count - 3)
                {
                    continue;
                }

                Mine mine = GetMine(ix, iy);

                // don't open marked ones
                if (mine != null && mine.MineData.IsMarked)
                {
                    continue;
                }

                Open(mine);
            }
        }
    }



}
