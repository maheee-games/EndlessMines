﻿using UnityEngine;


public class Mine : MouseEventReceiver
{
    public MineData MineData { get; set; }

    private SpriteRenderer spriteRenderer;
    private Animator animator;

    void Start()
    {
    }

    void Update()
    {
    }

    override public void OnMouseLeftClicked()
    {
        MineData.Level.OnMineLeftClicked(this);
    }

    override public void OnMouseRightClicked()
    {
        MineData.Level.OnMineRightClicked(this);
    }

    override public void OnMouseMiddleClicked()
    {
        MineData.Level.OnMineMiddleClicked(this);
    }

    public void Initialize(MineData mineData, Transform parent, float translateX, float translateY)
    {
        MineData = mineData;

        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();

        spriteRenderer.sprite = mineData.CoverSprite;
        gameObject.transform.SetParent(parent, false);
        gameObject.transform.Translate(translateX, translateY, 0f);
    }

    public void Open()
    {
        MineData.IsOpen = true;
        animator.Play("Rotate_Away");
    }

    public void ToggleMark()
    {
        if (MineData.IsMarked)
        {
            MineData.IsMarked = false;
            spriteRenderer.sprite = MineData.CoverSprite;
        }
        else
        {
            MineData.IsMarked = true;
            spriteRenderer.sprite = MineData.MarkedSprite;
        }

    }

    public void OnAnimationEventAway()
    {
        spriteRenderer.sprite = MineData.OpenSprite;
        MineData.Level.OnMineOpening(this);
    }

}
